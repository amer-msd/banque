package com.sesame.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sesame.entities.Compte;

import java.util.Optional;

public interface CompteRepository extends JpaRepository<Compte, String> {

    Optional<Compte> findById(String codeCompte);
}
